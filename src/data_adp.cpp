//Implementazione del modulo data_adp
//converte il valore booleano in ingresso in una parola di 16 bit
//tutta 1 o tutta 0

#include <systemc.h>
#include "data_adp.hpp"

using namespace std;

void data_adp::convert()
{
	if (pwm_in.read() == '1')
		pwm_out.write(65535);
	else
		pwm_out.write(0);
}



