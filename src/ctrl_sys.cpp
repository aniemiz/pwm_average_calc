//Corpo del modulo ctrl_sys

#include <systemc.h>
#include "ctrl_sys.hpp"


//Metodo che restituisce il valore della variabile booleana result
bool ctrl_sys::check() 
{
	return(result);
}

//Thread che genera i segnali di controllo
void ctrl_sys::control() 
{
	//Scrive sulla porta i valori della memoria (simulata)	
	memory[0].write(1);
	memory[1].write(32767);
	memory[3].write(1);
	memory[4].write(1);
	memory[5].write(1);
	memory[6].write(1);
	memory[7].write(1);
	memory[8].write(1);
	memory[9].write(1);
	memory[10].write(1);
	memory[11].write(1);
	memory[12].write(1);
	memory[13].write(1);
	memory[14].write(1);
	memory[15].write(1);

	memory_address.write("0001");

	request.write(0);

	sync_pwm.write(1);

	wait(509, SC_NS);
	
	request.write(1);

	wait(1, SC_NS);
	
	request.write(0);
	
	wait(2, SC_NS);
	
	sync_pwm.write(0);
	
	//
	
	wait(65024, SC_NS);
	
	sync_pwm.write(1);
	memory_address.write("0002");
	
	wait(509, SC_NS);
	
	request.write(1);

	wait(1, SC_NS);
	
	request.write(0);
	
	wait(2, SC_NS);
	
	sync_pwm.write(0);
	
	//
	
	wait(65024, SC_NS);
	
	sync_pwm.write(1);
	memory_address.write("0003");
	
	wait(509, SC_NS);
	
	request.write(1);

	wait(1, SC_NS);
	
	request.write(0);
	
	wait(2, SC_NS);
	
	sync_pwm.write(0);
	
	//
	
	wait(65536, SC_NS);
	
	sc_stop(); //termina la simulazione
	
	/*
	do
	{
		wait();
		n++;
		if (n<512)
		sync_pwm.write(1);
		else
		sync_pwm.write(0);
		if (n == 509)
		request.write(1);
		else
		request.write(0);

	}
	while ();
	*/

}

/*
//Thread contatore
void ctrl_sys::counter() 
{
	
				
	while (true)
	{
				 		
	} 



}
*/

