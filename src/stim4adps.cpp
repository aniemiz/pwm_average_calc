//Corpo del generatore di stimoli per i noduli adattatori

#include <systemc.h>
#include "stim4adps.hpp"

using namespace std;

void stim4adps::generate() 
{
	//scrive un valore sulla porta di uscita
	num.write(53720); 
	cout << "Stim4adps @ " << sc_time_stamp() 
		 <<"\nScrive sulla porta di uscita num : 53720"<< endl;
	//attende il fronte positivo del clock
	cout << "Attende il clock...";
	wait();	
	cout << "FATTO." << endl;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nLegge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (I.read()!= 419)
		result = true; 
		 
	//scrive un valore sulla porta di uscita
	num.write(29215); 
	cout << "Stim4adps @ " << sc_time_stamp() 
		 <<"\nScrive sulla porta di uscita num : 29215"<< endl;
	//attende il fronte positivo del clock
	cout << "Attende il clock...";
	wait();	
	cout << "FATTO." << endl;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nLegge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (I.read()!= 228)
		result = true; 
	
	//scrive un valore sulla porta di uscita
	num.write(3396465); 
	cout << "Stim4adps @ " << sc_time_stamp() 
		 <<"\nScrive sulla porta di uscita num : 3396465"<< endl;
	//attende il fronte positivo del clock
	cout << "Attende il clock...";
	wait();	
	cout << "FATTO." << endl;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nLegge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (I.read()!= 26534)
		result = true; 
	
	//scrive un valore sulla porta di uscita
	num.write(804736); 
	cout << "Stim4adps @ " << sc_time_stamp() 
		 <<"\nScrive sulla porta di uscita num : 804736"<< endl;
	//attende il fronte positivo del clock
	cout << "Attende il clock...";
	wait();	
	cout << "FATTO." << endl;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nLegge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (I.read()!= 6287)
		result = true; 
	
	//scrive un valore sulla porta di uscita
	num.write(65535); 
	cout << "Stim4adps @ " << sc_time_stamp() 
		 <<"\nScrive sulla porta di uscita num : 65535"<< endl;
	//attende il fronte positivo del clock
	cout << "Attende il clock...";
	wait();	
	cout << "FATTO." << endl;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nLegge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (I.read()!= 511)
		result = true;
	
	//scrive un valore sulla porta di uscita
	num.write(8388607); 
	cout << "Stim4adps @ " << sc_time_stamp() 
		 <<"\nScrive sulla porta di uscita num : 8388607"<< endl;
	//attende il fronte positivo del clock
	cout << "Attende il clock...";
	wait();	
	cout << "FATTO." << endl;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nLegge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (I.read()!= 65535)
		result = true;
	
	cout << "\nStop simulazione." << endl;
	sc_stop(); //termina la simulazione
	cout << "result = " << result << endl;

}

//Metodo che restituisce il valore della variabile booleana result
bool stim4adps::check() 
{
	return(result);
}

