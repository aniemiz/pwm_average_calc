//Corpo del generatore di stimoli per il test del modulo PWM_AVG_CALC

#include <systemc.h>
#include "stim4pwmavgcalc.hpp"

using namespace std;

//Metodo che restituisce il valore della variabile booleana result
bool stim4pwmavgcalc::check() 
{
	return(result);
}

//Thread che genera i dati per il DUT
void stim4pwmavgcalc::generate() 
{
	//Pone a 0 il segnale di sincronismo col periodo PWM
	pwm_sync.write(0);		
	//Manda in uscita lo stesso dato 128 volte
	cout << "Stim4pwmavgcalc @ " << sc_time_stamp()
			 << "\nScrive sulla porta di ingresso il valore 130 per 128 volte" << endl;
	for (int i=1; i<=128; i++)
	{
		//scrive il valore sulla porta di uscita
		data_out.write(130);
		cout << "*"; 
		//Pone a 1 il segnale di sincronismo col periodo PWM se sta fornendo l'ultimo dato
		if (i == 128)
		pwm_sync.write(1);
		//attende il fronte positivo del clock	
		wait();	
	}
	cout <<  "\nStimuls4Reg @ " << sc_time_stamp();
	//Azzera tutte le uscite e attende il clock per leggere il risultato del calcolo
	//il risultato del calcolo ha una colpo ti clock come tempo di propagazione
	pwm_sync.write(0);
	data_out.write(0);
	wait();	
	//legge la porta di ingresso e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nIl valore della media dei 128 valori è: " << avg_in.read().to_int() << endl << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (avg_in.read()!= 130)
		result = true; 
		 
	
//Pone a 0 il segnale di sincronismo col periodo PWM
	pwm_sync.write(0);		
	//Manda in uscita lo stesso dato 128 volte
	cout << "Stim4pwmavgcalc @ " << sc_time_stamp()
			 << "\nScrive sulla porta di ingresso il valore 500 per 128 volte" << endl;
	for (int i=1; i<=128; i++)
	{
		//scrive il valore sulla porta di uscita
		data_out.write(500);
		cout << "*"; 
		//Pone a 1 il segnale di sincronismo col periodo PWM se sta fornendo l'ultimo dato
		if (i == 128)
		pwm_sync.write(1);
		//attende il fronte positivo del clock	
		wait();	
	}
	cout <<  "\nStimuls4Reg @ " << sc_time_stamp(); 
	//Azzera tutte le uscite e attende il clock per leggere il risultato del calcolo
	//il risultato del calcolo ha una colpo ti clock come tempo di propagazione
	pwm_sync.write(0);
	data_out.write(0);
	wait();	
	//legge la porta di ingresso e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nIl valore della media dei 128 valori è: " << avg_in.read().to_int() << endl << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (avg_in.read()!= 500)
		result = true; 

	//Pone a 0 il segnale di sincronismo col periodo PWM
	pwm_sync.write(0);		
	//Manda in uscita lo stesso dato 128 volte
	cout << "Stim4pwmavgcalc @ " << sc_time_stamp()
			 << "\nScrive sulla porta di ingresso il valore 822 per 128 volte" << endl;
	for (int i=1; i<=128; i++)
	{
		//scrive il valore sulla porta di uscita
		data_out.write(822);
		cout << "*"; 
		//Pone a 1 il segnale di sincronismo col periodo PWM se sta fornendo l'ultimo dato
		if (i == 128)
		pwm_sync.write(1);
		//attende il fronte positivo del clock	
		wait();	
	}
	cout <<  "\nStimuls4Reg @ " << sc_time_stamp(); 
	//Azzera tutte le uscite e attende il clock per leggere il risultato del calcolo
	//il risultato del calcolo ha una colpo ti clock come tempo di propagazione
	pwm_sync.write(0);
	data_out.write(0);
	wait();	
	//legge la porta di ingresso e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() 
		 << "\nIl valore della media dei 128 valori è: " << avg_in.read().to_int() << endl << endl;
	//Verifica la correttezza del dato restituito dagli adattatori
	if (avg_in.read()!= 822)
		result = true; 

	
	
	cout << "\nStop simulazione.\nStim4pwmavgcalc @ " << sc_time_stamp() << endl;
	sc_stop(); //termina la simulazione
	cout << "result = " << result << endl;

}


