//Corpo del monitor

#include <systemc.h>
#include "monitor.hpp"

using namespace std;

void monitor::print()
{
	//Per i threads, serve il while true loop, altrimenti esegue solo la prima volta
	while (true) 
	{
		//attende il clock e poi stampa a video il valore della porta
		wait();
		cout << "Monitor @ " << sc_time_stamp() << " I = " << I.read().to_int() << endl;
		
	}
}

