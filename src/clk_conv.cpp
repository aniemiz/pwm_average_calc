//Implementazione del modulo clk_conv
//converte il clock in un segnale con frequenza 512 volte minore

#include <systemc.h>
#include "clk_conv.hpp"

using namespace std;

void clk_conv::count()
{
	while (true)
	{
		//Scrive sulla porta di uscita
		clk_out.write(out);
		//Attende il fronte del clock
		wait();
		
		n++;
		if (n==256)
		{
			n = 0;
			out = !out;
		}		
		
	}
}



