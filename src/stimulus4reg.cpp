//Corpo del generatore di stimoli per il nodulo reg

#include <systemc.h>
#include "stimulus4reg.hpp"

using namespace std;

void stimulus4reg::generate() 
{
	//result = false; già fatto nel costruttore
	cl.write(false);
	en.write(true);
	//scrive un valore sulla porta di uscita
	num.write(100); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : 100"<< endl;
	//attende il fronte positivo del clock
	wait();	
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//scrive un valore sulla porta di uscita
	num.write(200); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : 200 "<< endl;
	//attende il fronte positivo del clock
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != 100)
	result = true;
  //legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//scrive un valore sulla porta di uscita
	num.write(250); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : 250 "<< endl;
	//attende il fronte positivo del clock
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != 200)
	result = true;
  //legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//scrive un valore sulla porta di uscita
	num.write(50); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : 50 "<< endl;	
	//attende il fronte positivo del clock
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != 250)
	result = true;
  //legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//scrive un valore sulla porta di uscita
	num.write(-500); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : -500 "<< endl;
	//attende il fronte positivo del clock
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != 50)
	result = true;
  //legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//scrive un valore sulla porta di uscita
	num.write(-7); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : -7 "<< endl;
	//attende il fronte positivo del clock
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != -500)
	result = true;
  //legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//resetta il registro
	cl.write(true);
	//scrive un valore sulla porta di uscita
	num.write(-88); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Attiva il segnale di clear "<< endl;
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : -88 "<< endl;
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != -7)
	result = true;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;	
	cl.write(false);
	//scrive un valore sulla porta di uscita
	num.write(-88); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Disttiva il segnale di clear "<< endl;
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : -88 "<< endl;
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != 0)
	result = true;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	//Disattiva enable
	en.write(false);
	//scrive un valore sulla porta di uscita
	num.write(-77); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Disttiva il segnale di enable "<< endl;
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : -77 "<< endl;
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != -88)
	result = true;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;

	//Attiva enable
	en.write(true);
	//scrive un valore sulla porta di uscita
	num.write(-55); 
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Attiva il segnale di enable "<< endl;
	cout << "Stimuls4Reg @ " << sc_time_stamp() <<" Scrive sulla porta di uscita num : -55 "<< endl;
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != -88)
	result = true;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	wait();
	//se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != -55)
	result = true;
	//legge la porta e stampa a video il valore
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di uscita num = " << num.read().to_int() << endl;
	cout <<  "Stimuls4Reg @ " << sc_time_stamp() << " Legge dalla porta di ingresso I = " << I.read().to_int() << endl;
	sc_stop(); //termina la simulazione
}

//Metodo che restituisce il valore della variabile booleana result
bool stimulus4reg::check() 
{
	return(result);
}
