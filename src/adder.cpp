//Corpo del sommatore a 16 bit

#include <systemc.h>
#include "adder.hpp"

void adder::compute()
{
	//NB è meglio usare i metodi read e write per accedere e scrivere sulle porte
	//la conversione del tipo di dato è automatica
	o->write(a->read().to_int() + b->read().to_int());
}


