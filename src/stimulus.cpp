//Corpo del generatore di stimoli

#include <systemc.h>
#include "stimulus.hpp"

using namespace std;

void stimulus::generate() 
{
	result = false;
	//scrive i valori sulle porte di uscita
	a.write(100); 
	b.write(200);
	//attende il fronte positivo del clock
	wait();	
	//legge le porte e stampa a video il valore e ...
	cout << "a = " << a.read().to_int() << endl;
	cout << "b = " << b.read().to_int() << endl;
	//Se il risultato è sbagliato resetta il flag
	if (I.read().to_int() != 300)
	result = true;
	//... scrive i nuovi valori sulle porte di uscita
	a.write(-100); 
	b.write(-200);
	//attende il clock ecc.
	wait();
	cout << "a = " << a.read().to_int() << endl;
	cout << "b = " << b.read().to_int() << endl;
	if (I.read().to_int() != -300)
	result = true;
	a.write(100); 
	b.write(-100);
	wait();
	cout << "a = " << a.read().to_int() << endl;
	cout << "b = " << b.read().to_int() << endl;
	if (I.read().to_int() != 0)
	result = true;
	a.write(200); 
	b.write(-50);
	wait();
	cout << "a = " << a.read().to_int() << endl;
	cout << "b = " << b.read().to_int() << endl;
	if (I.read().to_int() != 150)
	result = true;
	a.write(50); 
	b.write(-200);
	wait();
	cout << "a = " << a.read().to_int() << endl;
	cout << "b = " << b.read().to_int() << endl;
	if (I.read().to_int() != -150)
	result = true;
	sc_stop(); //termina la simulazione
}

//Metodo che restituisce il valore della variabile booleana result
bool stimulus::check() 
{
	return(result);
}
