//Corpo del registro
//NB: i segnali clear ed enable sono attivi alti

#include <systemc.h>
#include "reg.hpp"

using namespace std;

void reg::do_reg() 
{
		if (cl == true)
			w_out->write(0);
		else
		{
			//se è attivo enable porta l'ingresso in uscita
			if(en == true)
			w_out->write(w_in->read());	
		}
}

