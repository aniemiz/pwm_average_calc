//Interfaccia del modulo adattatore BIG2LITT da 23 bit a 16 bit
//NB: il presente modulo prende i 16 bit PIU' SIGNIFICATIVI della parola a 23 bit in ingresso

#ifndef BIG2LITT_HPP
#define BIG2LITT_HPP

#include <systemc.h>
	
	SC_MODULE(big2litt)
	{
		//Porte di ingresso uscita
		sc_in<sc_bv<23> > data_in;		//parola in ingresso
		sc_out<sc_bv<16> > data_out;	//parola in uscita
	
		//Metodo
		void convert()
		{
			data_out.write(data_in.read().range(22,7));
		}
		
		//Costruttore
		SC_CTOR(big2litt)
		{
			SC_METHOD(convert);
			sensitive << data_in;
		}
	};
	
	#endif
