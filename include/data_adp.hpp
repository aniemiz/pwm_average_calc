//Interfaccia del modulo adattatore per i dati pwm
//trasforma il segnale boobleano PWM in una parola a 16 bit 
//tutta 1 o tutta 0

#ifndef DATA_ADP_HPP
#define DATA_ADP_HPP

#include <systemc.h>

SC_MODULE(data_adp)
{
	private:

	//Metodi
	void convert();	
	
	public:
	
	//Porte di ingresso e uscita
	sc_in<sc_lv<1> > pwm_in;
	sc_out<sc_bv<16> > 	pwm_out;

	//COSTRUTTORE	
	SC_CTOR(data_adp) 
	{
		SC_METHOD(convert);
		sensitive << pwm_in;		
	}

};

#endif
