//Interfaccia del modulo adattatore LITT2BIG da 16 bit a 23 bit
//NB tale modulo scrive la parola a 16 bit in ingresso nei bit meno significativi 
//della parola a 23 bit in uscita

#ifndef LITT2BIG_HPP
#define LITT2BIG_HPP

#include <systemc.h>
	
	SC_MODULE(litt2big)
	{

		//Porte di ingresso uscita
		sc_in<sc_bv<16> > data_in;		//parola in ingresso
		sc_out<sc_bv<23> > data_out;	//parola in uscita
	
		//Metodo
		void convert()
		{
			data_out.write( concat("0000000", data_in.read()) );
		}
		
		//Costruttore
		SC_CTOR(litt2big)
		{
			SC_METHOD(convert);
			sensitive << data_in;
		}
	};
	
	#endif
