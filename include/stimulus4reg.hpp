//Interfaccia del generatore di stimoli per il test del modulo reg

#ifndef STIMULUS4REG_HPP
#define STIMULUS4REG_HPP

#include <systemc.h>

SC_MODULE (stimulus4reg) 
{
	//Porte
	sc_in_clk clk;
	sc_out<sc_bv<23> >  num;
	sc_in<sc_bv<23> > I;
	sc_out<bool> en, cl;
	
	//Variabili
	bool result;
	
	//Metodo
	void generate();
	
	//Metodo per restituire il risultato del test
	bool check();
	
	SC_CTOR (stimulus4reg) 
	{
		SC_CTHREAD(generate,clk.pos());	//sensibile al fonte positivo del clock
		result = false;	//assegnazione di default
	}
};

#endif
