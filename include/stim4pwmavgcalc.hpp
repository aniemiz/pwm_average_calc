//Generatore di stimoli per il test del modulo PWM_AVG_CALC

#ifndef STIM4PWMAVGCALC_HPP
#define STIM4PWMAVGCALC_HPP

#include <systemc.h>

SC_MODULE(stim4pwmavgcalc) 
{
	//Porte
	sc_in_clk 					clk;
	sc_out<sc_bv<16> >  data_out;
	sc_out<bool> 				pwm_sync;
	sc_in<sc_bv<16> > 	avg_in;
	
	//Variabili
	bool result;
	
	//Metodo
	void generate();
	
	//Metodo per restituire il risultato del test
	bool check();
	
	SC_CTOR (stim4pwmavgcalc) 
	{
		SC_CTHREAD(generate,clk.pos());	//sensibile al fonte positivo del clock
		result = false;	//assegnazione di default
	}
};

#endif
