//Interfaccia del modulo monitor
//tale modulo si occupa di stampare a video il valore della variabile
//in ingresso per ogni istante di clock

#ifndef MONITOR_HPP
#define MONITOR_HPP

#include <systemc.h>

SC_MODULE(monitor)
{
	//Porte
	sc_in_clk clk;
	sc_in<sc_bv<23> > I;
	
	//Metodo (che in questo caso è uno thread, cioé un modulo sequenziale)
	void print();
	
	//Costruttore
	SC_CTOR(monitor)
	{
		SC_CTHREAD(print, clk.pos());	//Sensibile al fronte positivo del clock
	}
};

#endif
