//Interfaccia di PWM_AVG_CALC

#ifndef PWM_AVG_CALC_HPP
#define PWM_AVG_CALC_HPP

#include <systemc.h>
#include "reg.hpp"
#include "adder.hpp"
#include "litt2big.hpp"
#include "big2litt.hpp"

SC_MODULE(pwm_avg_calc)
{

	//Porte di ingresso e uscita
	sc_in_clk 					clk;			//clock
	sc_in<sc_bv<16> > 	data_in;	//parola in ingresso
	sc_out<sc_bv<16> > 	avg_out;	//parola in uscita
	sc_in<bool> 				sync_en;	//segnale di sincronismo col periodo di pwm
		
	//Definizione dei segnali
	sc_signal<sc_bv<23> > 	data_in_sig;
	sc_signal<sc_bv<23> > 	data_out_sig;	
	
	sc_signal<sc_bv<23> > 	temps_sig;
	sc_signal<sc_bv<23> > 	summ_out_sig;
	
	sc_signal<bool> 				zero_bit;		//<<<<<<<===========
	sc_signal<bool> 				one_bit;

	//Variabili puntatori ai blocchi da istanziare	
	reg *REG_ACC;
	reg *REG_OUT;
	adder *ADD;
	litt2big *CONV_IN;
	big2litt *CONV_OUT;
	
	
	
//COSTRUTTORE

	SC_CTOR(pwm_avg_calc) 
	//richiama i costruttori dei vari moduli :REG_ACC("REG_ACC"), REG_OUT("REG_OUT"), ADD("ADD"), CONV_IN("CONV_IN"), CONV_OUT("CONV_OUT")
	{
		
		/*
		//Creazione delle istanze dei moduli (NB: allocati nello stack)
		reg 			REG_ACC("ACCUMULATOR_REG");
		reg 			REG_OUT("DATA_OUT_REG");
		adder 		ADD("ADDER");
		litt2big 	CONV_IN("IN_CONVERSION");
		big2litt 	CONV_OUT("OUT_CONVERSION");
		
		//Collegamento dei segnali alle porte dei moduli
		REG_ACC.clk (this->clk);
		REG_ACC.w_in (summ_out_sig);
		REG_ACC.w_out (temps_sig);
		REG_ACC.cl (this->sync_en);	
		REG_ACC.en (one_bit);
	
		ADD.a (temps_sig);
		ADD.b (data_in_sig); 
		ADD.o (summ_out_sig);

		REG_OUT.clk (this->clk);
		REG_OUT.w_in (summ_out_sig);	
		REG_OUT.w_out (data_out_sig);
		REG_OUT.en (this->sync_en);
		REG_OUT.cl (zero_bit);
		
		CONV_IN.data_in (this->data_in);
		CONV_IN.data_out (data_in_sig);
		
		CONV_OUT.data_in (data_out_sig);
		CONV_OUT.data_out (this->avg_out);
		*/
		
		//Creazione delle istanze dei moduli (NB: allocati nella memoria)
		//e collegamento degli stessi
		REG_ACC = new reg("ACCUMULATOR_REG");
		REG_ACC->clk(clk);
		REG_ACC->w_in(summ_out_sig);
		REG_ACC->w_out(temps_sig);
		REG_ACC->cl(sync_en);	
		REG_ACC->en(one_bit);
		
		REG_OUT = new reg("DATA_OUT_REG");
		REG_OUT->clk(clk);
		REG_OUT->w_in(summ_out_sig);	
		REG_OUT->w_out(data_out_sig);
		REG_OUT->en(sync_en);
		REG_OUT->cl(zero_bit);
		
		ADD = new adder("ADDER");
		ADD->a(temps_sig);
		ADD->b(data_in_sig); 
		ADD->o(summ_out_sig);
		
		CONV_IN = new litt2big("IN_CONVERSION");
		CONV_IN->data_in(data_in);
		CONV_IN->data_out(data_in_sig);
		
		CONV_OUT = new big2litt("OUT_CONVERSION");
		CONV_OUT->data_in(data_out_sig);
		CONV_OUT->data_out(avg_out);
		
		//Definizione segnali costanti		<<<<<<<<==========
		zero_bit = false;
		one_bit = true;
	}	
	
	
	
};

#endif
