//Interfaccia del registro a 23 bit

#ifndef REG_HPP
#define REG_HPP

#include <systemc.h>

SC_MODULE(reg)
{
	//Porte di ingresso e uscita
	sc_in_clk clk;							//clock
	sc_in<sc_bv<23> > w_in;			//parola in ingresso
	sc_out<sc_bv<23> > w_out;		//parola in uscita
	sc_in<bool> cl;						//clear
	sc_in<bool> en;						//enable
	
	//metodi
	void do_reg();							//per registro a 23 bit
	
	//COSTRUTTORE
	SC_CTOR(reg)
	{
		SC_METHOD(do_reg);
		sensitive << clk.pos();
		w_out.initialize(0);
	}
	
};

#endif
