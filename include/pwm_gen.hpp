//INTERFACCIA del modulo pwm_gen che utilizza moduli del repo di Bandiziol
//Lo scopo è quello di creare una entità che raggruppi le parti del generatore pwm
//scritto da Bandiziol

#ifndef PWM_GEN_HPP
#define PWM_GEN_HPP

#include <systemc.h>
#include <iostream>
#include "interface.hpp"
#include "timer.hpp"
#include "comparator.hpp"
#include "flip_flop_sr.hpp"

#ifndef	MEMORY_SIZE
#define MEMORY_SIZE 16
#endif

//using namespace std;

SC_MODULE(pwm_gen)
{

	//Porte di ingresso e uscita	
	sc_in<sc_bv<4> > memory_address;
	sc_in<sc_bv<16> > memory[MEMORY_SIZE];		//<<<<<<<<<<<<<<<<<
	sc_in<bool> request;
	sc_in<bool> clock;
	sc_out<sc_lv<1> > output;
		
	//Definizione dei segnali
	sc_signal<bool> reset;
	sc_signal<bool> start;
	sc_signal<bool> transferring;
	sc_signal<sc_bv<16> > data1;
	sc_signal<sc_bv<16> > data2;
	sc_signal<bool> one_bigger;
	sc_signal<bool> two_bigger;
	sc_signal<bool> waiting_for_timer;

	//Variabili puntatori ai blocchi da istanziare	
	Interface *i1;
	Timer *tim1;
	Comparator *c1;
	Flip_Flop_SR *f1;
	
	
//COSTRUTTORE

	SC_CTOR(pwm_gen) 
	{
		//Creazione delle istanze dei moduli (NB: allocati nella memoria)
		//e collegamento degli stessi
		
		i1 = new Interface("Interface");		
		i1->memory_address(memory_address);
		i1->reset(reset);
		i1->memory[0](memory[0]);
		i1->memory[1](memory[1]);
		i1->memory[2](memory[2]);
		i1->memory[3](memory[3]);
		i1->memory[4](memory[4]);
		i1->memory[5](memory[5]);
		i1->memory[6](memory[6]);
		i1->memory[7](memory[7]);
		i1->memory[8](memory[8]);
		i1->memory[9](memory[9]);
		i1->memory[10](memory[10]);
		i1->memory[11](memory[11]);
		i1->memory[12](memory[12]);
		i1->memory[13](memory[13]);
		i1->memory[14](memory[14]);
		i1->memory[15](memory[15]);
		i1->start(start);
		i1->transferring(transferring);
		i1->data(data1);
		i1->clock(clock);
		i1->request(request);
		i1->waiting_for_timer(waiting_for_timer);
		
		tim1 = new Timer("Timer");		
		tim1->clock(clock);
		tim1->start(start);
		tim1->transferring(transferring);
		tim1->counter(data2);
		tim1->reset(reset);
		tim1->waiting_for_timer(waiting_for_timer);

		c1 = new Comparator("Comparator");
		c1->input1(data1);
		c1->input2(data2);
		c1->one_bigger(one_bigger);
		c1->two_bigger(two_bigger);
	
		f1 = new Flip_Flop_SR("Flip_Flop_SR");
		f1->set(one_bigger);
		f1->reset(two_bigger);
		f1->output(output);
	}	
		
};


#endif
