//Interfaccia del generatore di stimoli

#ifndef STIMULUS_HPP
#define STIMULUS_HPP

#include <systemc.h>

SC_MODULE (stimulus) 
{
	//Porte
	sc_in_clk clk;
	sc_out<sc_bv<23> > a;
	sc_out<sc_bv<23> > b;
	sc_in<sc_bv<23> > I;
	
	//Variabili
	bool result;
	
	//Metodo (che in questo caso è uno thread, cioé un modulo sequenziale)
	void generate();
	
	//Metodo per restituire il risultato del test
	bool check();
	
	SC_CTOR (stimulus) 
	{
		SC_CTHREAD(generate,clk.pos());
		//sensitive << clk.pos();		//sensibile al fonte positivo del clock
	}
};

#endif
