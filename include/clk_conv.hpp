//Interfaccia del modulo adattatore per il segnale di clock
//divide la frequenza del clock per 512

#ifndef CLK_CONV_HPP
#define CLK_CONV_HPP

#include <systemc.h>

SC_MODULE(clk_conv)
{
	private:
	
	//Variabili locali
	int n;
	bool out;
	
	//Metodi
	void count();

	public:
	
	//Porte di ingresso e uscita
	sc_in_clk clk_in;
	sc_out<bool> clk_out;
	
	//COSTRUTTORE	
	SC_CTOR(clk_conv) 
	{
		SC_THREAD(count);
		sensitive << clk_in.pos();		
		
		out = true;
		n = 0;
	}

};

#endif
