//Interfaccia del sommatore a 23 bit

#ifndef ADDER_HPP
#define ADDER_HPP
//Servono per indicare al compilatore di includere il codice una volta sola

#include <systemc.h>

//Definizione del modulo ADDER (NB: è come una classe)
SC_MODULE(adder)
{
	//Porte di ingresso e uscita
	sc_in<sc_bv<23> > a;
	sc_in<sc_bv<23> > b;
	sc_out<sc_bv<23> > o;
	
	//Dichiarazione del metodo che fa il calcolo
	void compute();
	
	//COSTRUTTORE
	SC_CTOR(adder)
	{
		//Costruzione del metodo
		SC_METHOD(compute);
		//Lista di sensitività
		sensitive << a << b;
	}
	
};

#endif
