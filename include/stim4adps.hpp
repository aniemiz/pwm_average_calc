//Generatore di stimoli per il test degli adattatori

#ifndef STIM4ADPS_HPP
#define STIM4ADPS_HPP

#include <systemc.h>

SC_MODULE(stim4adps) 
{
	//Porte
	sc_in_clk 			clk;
	sc_out<sc_bv<23> >  num;
	sc_in<sc_bv<23> > 	I;
	
	//Variabili
	bool result;
	
	//Metodo
	void generate();
	
	//Metodo per restituire il risultato del test
	bool check();
	
	SC_CTOR (stim4adps) 
	{
		SC_CTHREAD(generate,clk.pos());	//sensibile al fonte positivo del clock
		result = false;	//assegnazione di default
	}
};

#endif
