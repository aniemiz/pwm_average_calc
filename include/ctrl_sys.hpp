//Interfaccia del modulo Control System che ha il compito di generare i segnali 
//di stimolo per il modulo pwm_gen e di controllare la correttezza del risultato 
//del calcolo della media effettuato dal modulo pwm_avg_calc

#ifndef CTRL_SYS_HPP
#define CTRL_SYS_HPP

#include <systemc.h>

#ifndef	MEMORY_SIZE
#define MEMORY_SIZE 16
#endif


SC_MODULE(ctrl_sys)
{
	private:
	
	//Variabili
	bool result;
	int n;
	
	public:
	
	//Metodi
	void control();
	//void counter();
	bool check();
	
	//Segnali
	//sc_signal<bool> gigi;
	
	//Porte di ingresso e uscita	
	sc_in_clk clk;
	sc_in<sc_bv<16> > avg_calc;
	sc_out<sc_bv<4> > memory_address;
	sc_out<sc_bv<16> > memory[MEMORY_SIZE];
	sc_out<bool> request;
	sc_out<bool> sync_pwm;
	
	//COSTRUTTORE
	SC_CTOR(ctrl_sys)
	{
		SC_CTHREAD(control,clk.pos());	
		//SC_CTHREAD(counter,clk.pos());	
		result = false;	
		n = 0;
	}

};

#endif

