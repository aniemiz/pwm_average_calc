//Programma di test per i moduli LITT2BIG e BIG2LITT

#include <systemc.h>
#include "stim4adps.hpp"
#include "litt2big.hpp"
#include "big2litt.hpp"
#include "monitor.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{
	cout << "=> Definisce i segnali" << endl;	
	
	//Definizione dei segnali
	sc_signal<sc_bv<23> > num_sig;	
	sc_signal<sc_bv<16> > litt_sig;
	sc_signal<sc_bv<23> > I_sig;
	
	sc_clock testclk;

	cout << "=> Istanzia i moduli" << endl;	

	//Creazione delle istanze dei moduli
	stim4adps STIM("STIM");
	big2litt DUT1("DUT1");
	litt2big DUT2("DUT2");
	//monitor MON("MON");
	
	cout << "=> Collega i moduli" << endl;		
	
	//Collegamento dei segnali alle porte dei moduli
	STIM.num (num_sig);
	STIM.clk (testclk);
	STIM.I (I_sig);

	DUT1.data_in (num_sig);
	DUT1.data_out (litt_sig);

	DUT2.data_in (litt_sig);
	DUT2.data_out (I_sig);
	
	//MON.I (I_sig);
	//MON.clk (testclk);
	
	cout << "\nStart simulation\n" << endl;		
	
	//Partenza simulazione
	sc_start();

return STIM.check();
}

