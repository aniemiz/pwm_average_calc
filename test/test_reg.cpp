//Programma di test per modulo reg

#include <systemc.h>
#include "reg.hpp"
#include "stimulus4reg.hpp"
#include "monitor.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{
	cout << "Definisce i segnali" << endl;	
	
	//Definizione dei segnali
	sc_signal<sc_bv<23> > num_sig;
	sc_signal<sc_bv<23> > I_sig;
	sc_signal<bool> cl_sig;
	sc_signal<bool> en_sig;
	//sc_signal<bool> testclk;
	sc_clock testclk;

	cout << "Istanzia i moduli" << endl;	

	//Creazione delle istanze dei moduli
	reg DUT("DUT");
	stimulus4reg STIM("STIM");
	monitor MON("MON");
	
	cout << "Collega i moduli" << endl;		
	
	//Collegamento dei segnali alle porte dei moduli
	DUT.w_in (num_sig);
	DUT.w_out (I_sig);
	DUT.en (en_sig);
	DUT.cl (cl_sig);
	DUT.clk (testclk);
	
	STIM.num (num_sig);
	STIM.en (en_sig);
	STIM.cl (cl_sig);
	STIM.clk (testclk);
	STIM.I (I_sig);
	
	MON.I (I_sig);
	MON.clk (testclk);
	
	cout << "Start simulation" << endl;		
	
	//Partenza simulazione
	sc_start();
	
/*APPUNTI
	//Genera il segnale di clock
	while(true)
	{
		testclk = 0; 
		sc_start(1);
		testclk = 1; 
		sc_start(1);
	}

*/
	return STIM.check();
}

