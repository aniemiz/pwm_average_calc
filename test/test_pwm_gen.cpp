//Programma che rispecchia il funzionamnto del test top_level_tb di Bandiziol
//ma che utilizza il modulo gen_pwm creato a partire dai componenti progetttati da Bandiziol

#include <systemc.h>
#include <iostream>
#include "top_level.hpp"
#include "pwm_gen.hpp"


using namespace std;

int sc_main (int argc, char* argv[]) 
{
	//Definizione dei segnali
	sc_clock clock("clock");
	sc_signal<sc_bv<4> > memory_address;
	sc_signal<sc_bv<16> > memory[MEMORY_SIZE];
	sc_signal<sc_lv<1> > output;
	sc_signal<bool> request;

	//Creazione delle istanze dei moduli
	Top_Level t1("Top_Level");
	pwm_gen pg1("Pwm_Gen"); 
	
	//Collegamento dei segnali alle porte dei moduli
	t1.memory_address(memory_address);
	t1.memory[0](memory[0]);
	t1.memory[1](memory[1]);
	t1.memory[2](memory[2]);
	t1.memory[3](memory[3]);
	t1.memory[4](memory[4]);
	t1.memory[5](memory[5]);
	t1.memory[6](memory[6]);
	t1.memory[7](memory[7]);
	t1.memory[8](memory[8]);
	t1.memory[9](memory[9]);
	t1.memory[10](memory[10]);
	t1.memory[11](memory[11]);
	t1.memory[12](memory[12]);
	t1.memory[13](memory[13]);
	t1.memory[14](memory[14]);
	t1.memory[15](memory[15]);
	t1.clock(clock);
	t1.output(output);
	t1.request(request);

	pg1.memory_address(memory_address);
	pg1.memory[0](memory[0]);
	pg1.memory[1](memory[1]);
	pg1.memory[2](memory[2]);
	pg1.memory[3](memory[3]);
	pg1.memory[4](memory[4]);
	pg1.memory[5](memory[5]);
	pg1.memory[6](memory[6]);
	pg1.memory[7](memory[7]);
	pg1.memory[8](memory[8]);
	pg1.memory[9](memory[9]);
	pg1.memory[10](memory[10]);
	pg1.memory[11](memory[11]);
	pg1.memory[12](memory[12]);
	pg1.memory[13](memory[13]);
	pg1.memory[14](memory[14]);
	pg1.memory[15](memory[15]);
	pg1.clock(clock);
	pg1.output(output);
	pg1.request(request);

	//Open VCD file
	sc_trace_file *wf = sc_create_vcd_trace_file("PWM");

	//Dump the desired signals
	sc_trace(wf, clock, "clock");
	sc_trace(wf, memory_address, "memory_address");
	sc_trace(wf, pg1.reset, "reset");
	sc_trace(wf, memory[0], "memory[0]");
	sc_trace(wf, pg1.start, "start");
	sc_trace(wf, pg1.transferring, "transferring");
	sc_trace(wf, pg1.data1, "data1");
	sc_trace(wf, pg1.data2, "data2");
	sc_trace(wf, pg1.one_bigger, "one_bigger");
	sc_trace(wf, pg1.two_bigger, "two_bigger");
	sc_trace(wf, output, "output");
	sc_trace(wf, request, "request");


	sc_start(SC_ZERO_TIME);
	sc_start(300000, SC_NS);

	sc_close_vcd_trace_file(wf);
	return 0;

}




