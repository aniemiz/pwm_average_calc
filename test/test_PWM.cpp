//Programma per testare il funzionamento del calcolatore della media con il generatore pwm
//vengono utilizzati anche i moduli adattatori e il generatore di stimoli ctrl_sys

#include <systemc.h>
#include "pwm_gen.hpp"
#include "pwm_avg_calc.hpp"
#include "ctrl_sys.hpp"
#include "clk_conv.hpp"
#include "data_adp.hpp"

using namespace std;


int sc_main(int argc, char* argv[])
{
	//Definizione dei segnali
	sc_clock 							testclk;
	sc_signal<bool> 			low_clk;
	sc_signal<sc_lv<1> >	output;
	sc_signal<bool> 			sync_pwm;
	sc_signal<sc_bv<16> > data_in;
	sc_signal<sc_bv<16> > avg_calc;
	sc_signal<sc_bv<4> > 	memory_address;
	sc_signal<sc_bv<16> > memory[MEMORY_SIZE];	
	sc_signal<bool> request;
	
	//Variabili puntatori ai blocchi da istanziare	
	pwm_gen *GEN;
	ctrl_sys *CTRL;
	pwm_avg_calc *CALC;
	clk_conv *CONV;
	data_adp *ADP;
	
	//Creazione delle istanze dei moduli nella memoria	
	GEN = new pwm_gen("PWM_GEN");
	CTRL = new ctrl_sys("CTRL_SYS");
	CALC = new pwm_avg_calc("PWM_AVG_CALC");
	CONV = new clk_conv("CLK_CONV");
	ADP = new data_adp("DATA_ADP");
	
	//Collegamento dei segnali alle porte dei moduli
	GEN->clock (testclk);
	GEN->memory_address (memory_address);
	GEN->memory[MEMORY_SIZE] (memory[MEMORY_SIZE]);
	GEN->request (request);
	GEN->output (output);
	
	CTRL->clk (testclk);
	CTRL->avg_calc (avg_calc);
	CTRL->memory_address (memory_address);
	CTRL->memory[MEMORY_SIZE] (memory[MEMORY_SIZE]);
	CTRL->request (request);
	CTRL->sync_pwm (sync_pwm);
	
	CALC->data_in (data_in);
	CALC->avg_out (avg_calc);
	CALC->sync_en (sync_pwm);
	CALC->clk (low_clk);
	
	CONV->clk_in (testclk);
	CONV->clk_out (low_clk);
	
	ADP->pwm_in (output);
	ADP->pwm_out (data_in);
	
	//Apre il file .vcd per memorizzare le tracce dei segnali di interesse
	sc_trace_file* wf = sc_create_vcd_trace_file("test_PWM_wf");
	//Indica i segnali da memorizzare
	sc_trace(wf, testclk, "clk");
	sc_trace(wf, memory[0], "memory[0]");
	sc_trace(wf, memory_address, "memory_address");
	sc_trace(wf, request, "request");
	sc_trace(wf, sync_pwm, "sync_pwm");
	sc_trace(wf, low_clk, "low_clk");
	sc_trace(wf, output, "output");
	sc_trace(wf, data_in, "data_in");
	sc_trace(wf, avg_calc, "avg_calc");
			
	//Partenza simulazione
	cout << "Start simulation.  Time = " << sc_time_stamp() << endl;
	sc_start();

	//Chiude il file delle forme d'onda memorizzate
	sc_close_vcd_trace_file(wf);
	return CTRL->check();
}

