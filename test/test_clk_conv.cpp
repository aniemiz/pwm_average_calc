//Programma di test per il funzionamnto del adattatore del clock

#include <systemc.h>
#include <iostream>
#include "clk_conv.hpp"

using namespace std;

int sc_main (int argc, char* argv[]) 
{
	//Definizione dei segnali
	sc_clock clk;
	sc_signal<bool> clk_out;
	
	//Creazione delle istanze dei moduli
	clk_conv apt("Clock_Adapter");
	
	//Collegamento dei segnali alle porte dei moduli
	apt.clk_in(clk);
	apt.clk_out(clk_out);
	
	//Apertura del file VCD
	sc_trace_file *wf = sc_create_vcd_trace_file("Clock_Adapter");
	//Definizione dei segnali da tracciare
	sc_trace(wf, clk, "clock");
	sc_trace(wf, clk_out, "Clock_Out");

	//Partenza simulazione
	//sc_start(SC_ZERO_TIME);
	sc_start(300000, SC_NS);
	
	sc_close_vcd_trace_file(wf);
	return 0;
}


