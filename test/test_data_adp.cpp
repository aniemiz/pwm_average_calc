//Programma di test per il funzionamnto del adattatore dei dati pwm

#include <systemc.h>
#include <iostream>
#include "data_adp.hpp"

using namespace std;

SC_MODULE(glove)
{
	//Porte
	sc_out<sc_lv<1> > out;
	sc_in_clk clk;
	
	//Metodo
	void trd()
	{
		if (clk == 1)
		out.write('1');
		else
		out.write('0');
	}

	SC_CTOR(glove) 
	{
		SC_METHOD(trd);
		sensitive << clk;
	}
		
};


int sc_main (int argc, char* argv[]) 
{
	//Definizione dei segnali
	sc_clock clk;
	sc_signal<sc_bv<16> > pwm_out;
	sc_signal<sc_lv<1> > out;
	
	//Creazione delle istanze dei moduli
	data_adp dadp("Data_Adapter");
	glove glv("lv_ADAPTER");
	
	//Collegamento dei segnali alle porte dei moduli
	dadp.pwm_in(out);
	dadp.pwm_out(pwm_out);
	
	glv.clk(clk);
	glv.out(out);
	
	//Apertura del file VCD
	sc_trace_file *wf = sc_create_vcd_trace_file("Data_Adapter");
	//Definizione dei segnali da tracciare
	sc_trace(wf, clk, "pwm_in");
	sc_trace(wf, pwm_out, "pwm_out");

	//Partenza simulazione
	//sc_start(SC_ZERO_TIME);
	sc_start(3000, SC_NS);
	
	sc_close_vcd_trace_file(wf);
	return 0;
	

}


