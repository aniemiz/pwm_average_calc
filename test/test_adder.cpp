//Programma di test per modulo adder, ma anche per il monitor e per il gen di stimoli...

#include <systemc.h>
#include "adder.hpp"
#include "stimulus.hpp"
#include "monitor.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{
	//Definizione dei segnali
	sc_signal<sc_bv<23> > a_sig;
	sc_signal<sc_bv<23> > b_sig;
	sc_signal<sc_bv<23> > o_sig;
	//sc_signal<bool> testclk;
	sc_clock testclk;

	//Creazione delle istanze dei moduli
	adder DUT("DUT");
	stimulus STIM("STIM");
	monitor MON("MON");
	
	//Collegamento dei segnali alle porte dei moduli
	DUT.a (a_sig);
	DUT.b (b_sig);
	DUT.o (o_sig);
	
	STIM.a (a_sig);
	STIM.b (b_sig);
	STIM.clk (testclk);
	STIM.I (o_sig);
	
	MON.I (o_sig);
	MON.clk (testclk);
	
	//Partenza simulazione
	sc_start();
	
/*APPUNTI
	//Genera il segnale di clock
	while(true)
	{
		testclk = 0; 
		sc_start(1);
		testclk = 1; 
		sc_start(1);
	}

*/
	return STIM.check();
}

