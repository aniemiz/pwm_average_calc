//Programma di test per modulo PWM_AVG_CALC
//Può essere usato come test automatico

#include <systemc.h>
#include "pwm_avg_calc.hpp"
#include "stim4pwmavgcalc.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{
	//Definizione dei segnali
	sc_signal<sc_bv<16> > data_sig;
	sc_signal<sc_bv<16> > avg_sig;
	sc_clock 							testclk;
	sc_signal<bool> 			sync_sig;
	
	//Creazione delle istanze dei moduli
	pwm_avg_calc DUT("DUT");
	stim4pwmavgcalc STIM("STIM");
	
	//Collegamento dei segnali alle porte dei moduli
	DUT.data_in (data_sig);
	DUT.avg_out (avg_sig);
	DUT.sync_en (sync_sig);
	DUT.clk (testclk);
	
	STIM.data_out (data_sig);
	STIM.avg_in (avg_sig);
	STIM.pwm_sync (sync_sig);
	STIM.clk (testclk);
	
	//Apre il file .vcd per memorizzare le tracce dei segnali di interesse
	sc_trace_file* wf = sc_create_vcd_trace_file("test_pwmavgcalc_wf");
	//Indica i segnali da memorizzare
	sc_trace(wf, testclk, "clk");
	sc_trace(wf, sync_sig, "sync_en");
	sc_trace(wf, data_sig, "data");
	sc_trace(wf, avg_sig, "avg");
	
		
	//Partenza simulazione
	cout << "Start simulation.  Time = " << sc_time_stamp() << endl;
	sc_start();

	//Chiude il file delle forme d'onda memorizzate
	sc_close_vcd_trace_file(wf);
	return STIM.check();
}

